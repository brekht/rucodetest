<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 06.06.17
 * Time: 21:43
 */

namespace app\models;
use yii\base\Model;

class SignupForm extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email','password'], 'required'],
            ['email', 'email'],
            ['email','unique','targetClass'=>'app\models\User'],
            ['password', 'string', 'min'=>4, 'max'=>10],
        ];
    }

    public function signup()
    {
        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        return $user->save();
    }

}