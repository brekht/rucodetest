
var x, y;
var tempX, tempY, tempW, tempH;

var windowH;
var headerH;
var topH;
var topLeftH;
var topRightH;
var leftH;
var rightH;
var bottomH;
var bottomLeftH;
var bottomRightH;

//window.onload = initBehavior;

function initBehavior()
{
    windowH = document.getElementById("window");
    //headerH = document.getElementById("header");
    headerH = document.getElementById("content");
    topH = document.getElementById("top-center");
    topLeftH = document.getElementById("top-left");
    topRightH = document.getElementById("top-right");
    leftH = document.getElementById("center-left");
    rightH = document.getElementById("center-right");
    bottomH = document.getElementById("bottom-center");
    bottomLeftH = document.getElementById("bottom-left");
    bottomRightH = document.getElementById("bottom-right");

    document.onmouseup = mouseUp;

    windowH.style.left = '100px';
    windowH.style.top = '100px';
    windowH.style.width = '100px';
    windowH.style.height = '100px';
    windowH.style.minWidth = '100px';
    windowH.style.minHeight = '100px';

    headerH.onmousedown = mouseDownHeader;
    headerH.onmouseup = mouseUp;
    topH.onmousedown = mouseDownTop;
    topH.onmouseup = mouseUp;
    topLeftH.onmousedown = mouseDownTopLeft;
    topLeftH.onmouseup = mouseUp;
    topRightH.onmousedown = mouseDownTopRight;
    topRightH.onmouseup = mouseUp;
    leftH.onmousedown = mouseDownLeft;
    leftH.onmouseup = mouseUp;
    rightH.onmousedown = mouseDownRight;
    rightH.onmouseup = mouseUp;
    bottomH.onmousedown = mouseDownBottom;
    bottomH.onmouseup = mouseUp;
    bottomLeftH.onmousedown = mouseDownBottomLeft;
    bottomLeftH.onmouseup = mouseUp;
    bottomRightH.onmousedown = mouseDownBottomRight;
    bottomRightH.onmouseup = mouseUp;
}
/*onmousedown*/
function mouseDown(e)
{
    tempX = parseInt(windowH.style.left);
    tempY = parseInt(windowH.style.top);
    tempW = parseInt(windowH.style.width);
    tempH = parseInt(windowH.style.height);
    x = e.clientX;
    y = e.clientY;
    return false;
}
function mouseDownHeader(e)
{
    document.onmousemove = mouseMoveHeader;
    return mouseDown(e);
}
function mouseDownTop(e)
{
    document.onmousemove = mouseMoveTop;
    return mouseDown(e);
}
function mouseDownTopLeft(e)
{
    document.onmousemove = mouseMoveTopLeft;
    return mouseDown(e);
}
function mouseDownTopRight(e)
{
    document.onmousemove = mouseMoveTopRight;
    return mouseDown(e);
}
function mouseDownLeft(e)
{
    document.onmousemove = mouseMoveLeft;
    return mouseDown(e);
}
function mouseDownRight(e)
{
    document.onmousemove = mouseMoveRight;
    return mouseDown(e);
}
function mouseDownBottom(e)
{
    document.onmousemove = mouseMoveBottom;
    return mouseDown(e);
}
function mouseDownBottomLeft(e)
{
    document.onmousemove = mouseMoveBottomLeft;
    return mouseDown(e);
}
function mouseDownBottomRight(e)
{
    document.onmousemove = mouseMoveBottomRight;
    return mouseDown(e);
}
/*mouseUp*/
function mouseUp()
{
    document.onmousemove = null;
}
/*mouseMove*/
function mouseMoveHeader(e)
{
    windowH.style.left = (tempX + e.clientX - x) + "px";
    windowH.style.top = (tempY + e.clientY - y) + "px";
    return false;
}
function mouseMoveTop(e)
{
    windowH.style.top = (tempY + e.clientY - y) + "px";
    windowH.style.height = (tempH + -(e.clientY - y)) + "px";
}
function mouseMoveTopLeft(e)
{
    windowH.style.top = (tempY + e.clientY - y) + "px";
    windowH.style.left = (tempX + e.clientX - x) + "px";
    windowH.style.width = (tempW + -(e.clientX - x)) + "px";
    windowH.style.height = (tempH + -(e.clientY - y)) + "px";
    return false;
}
function mouseMoveTopRight(e)
{
    windowH.style.top = (tempY + e.clientY - y) + "px";
    windowH.style.width = (tempW + e.clientX - x) + "px";
    windowH.style.height = (tempH + -(e.clientY - y)) + "px";
    return false;
}
function mouseMoveLeft(e)
{
    windowH.style.left = (tempX + e.clientX - x) + "px";
    windowH.style.width = (tempW + -(e.clientX - x)) + "px";
    return false;
}
function mouseMoveRight(e)
{
    windowH.style.width = (tempW + e.clientX - x) + "px";
    return false;
}
function mouseMoveBottom(e)
{
    windowH.style.height = (tempH + e.clientY - y) + "px";
    return false;
}
function mouseMoveBottomLeft(e)
{
    windowH.style.left = (tempX + e.clientX - x) + "px";
    windowH.style.width = (tempW + -(e.clientX - x)) + "px";
    windowH.style.height = (tempH + e.clientY - y) + "px";
    return false;
}
function mouseMoveBottomRight(e)
{
    windowH.style.width = (tempW + e.clientX - x) + "px";
    windowH.style.height = (tempH + e.clientY - y) + "px";
    return false;
}