<?php

namespace app\modules\breditor\models;

use yii;
use yii\db\ActiveRecord;

class Picture extends ActiveRecord
{
    public $image;

    public static function tableName()
    {
        return 'picture';
    }

    public function getFolder()
    {
        return Yii::getAlias('@web') . 'pictures/';
    }

    public function getRealFilename(){
        $ext = explode('.',$this->filename);
        $ext = array_pop($ext);
        return $this->id . '.' . $ext;
    }

    public function beforeDelete()
    {
        if(file_exists($this->getFolder() . $this->getRealFilename()))
        {
            unlink($this->getFolder() . $this->getRealFilename());
        }

        return parent::beforeDelete();
    }

    public function getFilenameParts()
    {
        $parts =  explode('.', $this->filename);
        $extension = array_pop($parts);
        $basename = implode('.', $parts);
        return ['basename'=>$basename, 'extension'=>$extension];
    }

}