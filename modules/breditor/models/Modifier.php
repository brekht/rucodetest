<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 14.06.17
 * Time: 10:54
 */

namespace app\modules\breditor\models;

class Modifier extends Picture
{
    private $modification;

    private function getImageFromFile($basename, $extension)
    {
        $image_resource = false;
        if(in_array(mb_strtolower($extension), ['jpg','jpeg'])){
            $image_resource = imagecreatefromjpeg($this->getFolder() . $basename . '.' . $extension);
        }
        elseif (mb_strtolower($extension) == 'png')
        {
            $image_resource = imagecreatefrompng($this->getFolder() . $basename . '.' . $extension);
        }
        return $image_resource;
    }

    private function setImageToFile($image_resource, $basename, $extension)
    {
        if(in_array(mb_strtolower($extension), ['jpg','jpeg'])){
            imagejpeg($image_resource, $this->getFolder() . $basename . '.' . $extension );
            imagedestroy($image_resource);
            return true;
        }
        elseif (mb_strtolower($extension) == 'png')
        {
            imagepng($image_resource, $this->getFolder() . $basename . '.' . $extension );
            imagedestroy($image_resource);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function modify($modification){
        $this->modification = $modification;
        switch ($this->modification['operation'])
        {
            case 'crop':
                $new = $this->imageCrop();
                break;
            case 'mirror':
                $new = $this->imageMirror();
                break;
            case 'convert':
                $new = $this->imageConvert();
                break;
            default:
                $new = false;
        }
        return $new;
    }

    public function imageCrop()
    {
        $name_parts = $this->getFilenameParts();
        $image = $this->getImageFromFile($this->id, $name_parts['extension']);
        $result_image = imagecrop($image,[
            'x'=>$this->modification['left'],
            'y'=>$this->modification['top'],
            'width'=>$this->modification['width'],
            'height'=>$this->modification['height']
        ]);
        $new_picture = new self();
        $new_picture->user_id = $this->user_id;
        $new_picture->filename = $this->filename;
        $new_picture->save();
        $this->setImageToFile($result_image, $new_picture->id, $name_parts['extension']);
        return $new_picture;
    }

    public function imageMirror()
    {
        $name_parts = $this->getFilenameParts();
        $image = $this->getImageFromFile($this->id, $name_parts['extension']);
        imageflip($image,IMG_FLIP_HORIZONTAL);
        $new_picture = new self();
        $new_picture->user_id = $this->user_id;
        $new_picture->filename = $this->filename;
        $new_picture->save();
        $this->setImageToFile($image, $new_picture->id, $name_parts['extension']);
        return $new_picture;
    }

    public function imageConvert()
    {
        $name_parts = $this->getFilenameParts();
        $conversion = in_array(mb_strtolower($name_parts['extension']), ['jpg','jpeg'])?'png':'jpg';
        $image = $this->getImageFromFile($this->id, $name_parts['extension']);
        $new_picture = new self();
        $new_picture->user_id = $this->user_id;
        $new_picture->filename = $name_parts['basename'] . '.' . $conversion;
        $new_picture->save();
        $this->setImageToFile($image, $new_picture->id, $conversion);
        return $new_picture;
    }
}