<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 14.06.17
 * Time: 5:45
 */

namespace app\modules\breditor\models;

use Yii;
use Yii\base\Model;

class UploadFormClient extends Model
{
    public $image;

    public function rule(){
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions'=>'jpg,png']
        ];
    }

    public function uploadPicture(){
        if($this->validate()){
            $picture = new Picture();
            $picture->user_id = Yii::$app->user->id;
            $picture->filename = $this->image->baseName . '.' . $this->image->extension;
            if($picture->save()){
                $this->image->saveAs($picture->getFolder() . $picture->getRealFilename());
                return $picture;
            }
        }
    }

}