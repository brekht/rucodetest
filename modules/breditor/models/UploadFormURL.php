<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 14.06.17
 * Time: 6:14
 */

namespace app\modules\breditor\models;

use Yii;
use Yii\base\Model;

class UploadFormURL extends Model
{
    public $image;

    public function rule(){
        return [
            [['image'], 'required'],
            [['image'], 'string'],
            [['image'], 'validateURL'],
        ];
    }

    public function validateURL(){
        $parsed_url = parse_url($this->image);
        $headers = @get_headers($this->image, 1);
        return ( !$parsed_url || !$headers || !preg_match('/^(HTTP)(.*)(200)(.*)/i', $headers[0]) );
    }

    public function uploadPicture(){
        if($this->validate()){
            $picture = new Picture();
            $picture->user_id = Yii::$app->user->id;
            $picture->filename = $this->getFilename();
            if($picture->save()){
                copy($this->image, $picture->getFolder() . $picture->getRealFilename());
                return $picture;
            }
        }
    }

    public function getFilename(){
        $parsed_url = parse_url($this->image);
        $fname = explode('/', $parsed_url['path']);
        return end($fname);
    }

}
