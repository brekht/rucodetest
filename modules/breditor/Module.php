<?php

namespace app\modules\breditor;

use yii\filters\AccessControl;
use yii;

class Module extends \yii\base\Module
{
    //public $layout='/breditor';

    public $controllerNamespace = 'app\modules\breditor\controllers';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function($rule, $action){
                    throw new \yii\web\NotFoundHttpException();
                },
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function($rule, $action)
                        {
                            return !Yii::$app->user->isGuest;
                        }
                    ]
                ]
            ]
        ];
    }

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
        //...
    }
}