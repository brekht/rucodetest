<?php
/**
 * Created by PhpStorm.
 * User: brekht
 * Date: 07.06.17
 * Time: 15:43
 */

namespace app\modules\breditor\controllers;

use app\modules\breditor\models\Modifier;
use app\modules\breditor\models\Picture;
use app\modules\breditor\models\UploadFormClient;
use app\modules\breditor\models\UploadFormURL;
use Yii;

use yii\web\Controller;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

class DefaultController extends Controller
{
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Picture::find()->where(['user_id' => Yii::$app->user->id]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $grid = GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'filename',
                ['class' => 'yii\grid\ActionColumn','template'=>'{view} {delete}'],
            ],
        ]);

        return $this->render('index',['grid'=>$grid]);
    }

    public function actionShowUploadForm(){
        return $this->render('upload',['client'=> new UploadFormClient(), 'url'=> new UploadFormURL()]);
    }

    public function actionUploadPictureClient(){
        $picture = new UploadFormClient();
        if(Yii::$app->request->isPost && $picture->load(Yii::$app->request->post())) {
            $picture->image = UploadedFile::getInstance($picture, 'image');
            return $this->render('view',['picture'=>$picture->uploadPicture()]);
        }
        return $this->render('upload',['client'=> $picture, 'url'=> new UploadFormURL()]);
    }

    public function actionUploadPictureUrl(){
        $picture = new UploadFormURL();
        if(Yii::$app->request->isPost && $picture->load(Yii::$app->request->post())) {
            $picture->image = $_POST['UploadFormURL']['image'];
            return $this->render('view',['picture'=>$picture->uploadPicture()]);
        }
        return $this->render('upload',['client'=> new UploadFormClient(), 'url'=> $picture]);
    }

    public function actionDelete($id)
    {
        $picture = Picture::findOne(['id'=>$id, 'user_id'=>Yii::$app->user->id]);
        $picture->delete();
        $this->redirect('index');
    }

    public function actionView($id)
    {
        $picture = Picture::findOne(['id'=>$id, 'user_id'=>Yii::$app->user->id]);
        if($picture) {
            return $this->render('view', ['picture' => $picture]);
        }
        $this->redirect('index');
    }

    public function actionModify($id){
        $modifier = Modifier::findOne(['id'=>$id, 'user_id'=>Yii::$app->user->id]);
        $new_picture = $modifier->modify(Yii::$app->getRequest()->getQueryParams());
        if($new_picture){
            return $this->render('view',['picture'=>$new_picture]);
        }else{
            $this->redirect('index');
        }
    }

    public function actionDownload($id)
    {
        $picture = Picture::findOne(['id'=>$id, 'user_id'=>Yii::$app->user->id]);

        $path = $picture->getFolder().$picture->getRealFilename();
        if (file_exists($path) && $picture->user_id == Yii::$app->user->id) {
            return Yii::$app->response->sendFile($path);
        }
    }
}