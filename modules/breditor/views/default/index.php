<?php
use yii\helpers\Html;

?>
<h1>Картинки</h1>

<div class="row">
    <div class="col-md-12">
        <?= Html::a('Добавить',['/breditor/default/show-upload-form'], ['class'=>'btn btn-primary']) ?>
        <?= $grid ?>
    </div>
</div>