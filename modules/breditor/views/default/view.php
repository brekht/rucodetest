<?php

?>
<h1><?= $picture->filename ?></h1>
<nav class="navbar navbar-default">
    <button type="button" id="crop-button" class="btn btn-default navbar-btn" onclick="crop();">Обрезать</button>
    <button type="button" class="btn btn-default navbar-btn" onclick="mirror();">Зеркалировать</button>
    <?php
        $filename_parts = $picture->getFilenameParts();
        if(in_array(mb_strtolower($filename_parts['extension']), ['jpg', 'jpeg'])):
    ?>
    <button type="button" class="btn btn-default navbar-btn" onclick="convert();">Экспортировать в PNG</button>
    <?php else: ?>
    <button type="button" class="btn btn-default navbar-btn" onclick="convert();">Экспортировать в JPEG</button>
    <?php endif; ?>
    <button type="button" class="btn btn-default navbar-btn" onclick="download();">Скачать бесплатно</button>

</nav>
<div class="well" style="height:500px; padding:50px; overflow: auto;">
    <div
        id="canvas"
        style="
            background-color: red;
            margin: auto;
            border: 4px solid black;
            display: inline-block;
            position: relative;
        "
    >
        <img src="<?= '/pictures/' . $picture->id . '.' . $filename_parts['extension'] ?>" alt="photo">
        <div id="window" style="display:none;">
            <div id="top">
                <div id="top-left">
                </div>
                <div id="top-right">
                </div>
                <div id="top-center">
                </div>
            </div>
            <div id="center">
                <div id="center-left">
                </div>
                <div id="center-right">
                </div>
                <div id="center-center">
                    <!--<div id="header">
                        header
                    </div>-->
                    <div id="content">
                    </div>
                </div>
            </div>
            <div id="bottom">
                <div id="bottom-left">
                </div>
                <div id="bottom-right">
                </div>
                <div id="bottom-center">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function crop(){
        var X = parseInt(windowH.style.left);
        var Y = parseInt(windowH.style.top);
        var W = parseInt(windowH.style.width);
        var H = parseInt(windowH.style.height);
        document.location.href="/breditor/default/modify?id=<?=$picture->id?>&operation=crop&left="+X+"&top="+Y+"&width="+W+"&height="+H;
    }
    function mirror(){
        document.location.href="/breditor/default/modify?id=<?=$picture->id?>&operation=mirror";
    }
    function convert(){
        document.location.href="/breditor/default/modify?id=<?=$picture->id?>&operation=convert";
    }
    function download(){
        document.location.href="/breditor/default/download?id=<?=$picture->id?>";
    }
</script>
<?php
$script = <<< JS
    document.croper = 0;
    $('#crop-button').prop('disabled', true);
    $('#canvas').on('click', function(){
        if (document.croper !== 1){
            document.croper = 1;
            $('#window').show(200, function(){
                initBehavior();
                $('#crop-button').prop('disabled', false);
            });
        }
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY);

$this->registerJsFile('/js/crop.js',  ['position' => yii\web\View::POS_END]);
?>