<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавление картинки';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<p>Загрузите картинку с Вашего компа</p>
<?php $form_client = ActiveForm::begin(['action'=>'/breditor/default/upload-picture-client', 'class'=>'form-horizontal']); ?>

<?= $form_client->field($client, 'image')->fileInput() ?>

<?= Html::submitButton('Загрузить', ['class'=>'btn btn-success']) ?>

<?php $form_client->end(); ?>
<br /><hr /><br />
<p>Либо загрузите из интернета</p>

<?php $form_url = ActiveForm::begin(['action'=>'/breditor/default/upload-picture-url', 'class'=>'form-horizontal']); ?>

<?= $form_url->field($url, 'image')->textInput() ?>

<?= Html::submitButton('Загрузить', ['class'=>'btn btn-success']) ?>

<?php $form_url->end(); ?>
