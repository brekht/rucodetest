<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:'.realpath(__DIR__."/../database")."/db.sqlite",
    'tablePrefix'=>'',
    'charset' => 'utf8',
];
