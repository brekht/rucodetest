<?php

use yii\db\Migration;

/**
 * Handles the creation of table `picture`.
 */
class m170608_074104_create_picture_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('picture', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
        ]);

        $this->createIndex(
            'idx-picture-user_id',
            'picture',
            'user_id'
        );
        /*
        $this->addForeignKey(
            'fk-picture-user_id',
            'picture',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        */
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /*
        $this->dropForeignKey(
            'fk-picture-user_id',
            'picture'
        );
        */
        $this->dropIndex(
            'idx-picture-user_id',
            'picture'
        );

        $this->dropTable('picture');
    }
}
